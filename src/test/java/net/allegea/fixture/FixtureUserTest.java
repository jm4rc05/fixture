package net.allegea.fixture;

import static junit.framework.Assert.assertNotNull;
import net.allegea.fixture.loader.FixtureFactoryLoader;
import net.allegea.fixture.model.User;

import org.junit.BeforeClass;
import org.junit.Test;

public class FixtureUserTest {

	@BeforeClass
	public static void setUp() {
		FixtureFactoryLoader.loadTemplates("net.allegea.fixture.template");
	}

	@Test
	public void fixtureAnyUser() {
		User user = Fixture.from(User.class).gimme("anyValidUser");
		assertNotNull("User should not be null", user);
	}

	@Test
	public void fixtureFemaleUser() {
		User user = Fixture.from(User.class).gimme("validFemaleUser");
		assertNotNull("User should not be null", user);
	}
	
}
