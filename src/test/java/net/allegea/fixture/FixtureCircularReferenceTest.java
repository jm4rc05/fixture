package net.allegea.fixture;

import static org.junit.Assert.assertTrue;
import net.allegea.fixture.loader.FixtureFactoryLoader;
import net.allegea.fixture.model.Item;
import net.allegea.fixture.model.Order;

import org.junit.BeforeClass;
import org.junit.Test;

public class FixtureCircularReferenceTest {

	@BeforeClass
	public static void setUp() {
		FixtureFactoryLoader.loadTemplates("net.allegea.fixture.template");
	}
	
	@Test
	public void circularReference() {
		Order order = Fixture.from(Order.class).gimme("valid");
		
		for (Item item : order.getItems()) {
			assertTrue("order relationship with item should have the same reference", item.getOrder() == order);
		}
		
		assertTrue("payment one-to-one relationship should have the same reference", order == order.getPayment().getOrder());
	}
	
	@Test
	public void circularReferenceEspecifyProperty() {
		Order order = Fixture.from(Order.class).gimme("otherValid");
		
		for (Item item : order.getItems()) {
			assertTrue("order relationship with item should have the same reference", item.getOrder() == order);
		}
		
		assertTrue("payment one-to-one relationship should have the same reference", order == order.getPayment().getOrder());
	}

}
