package net.allegea.fixture.template;

import net.allegea.fixture.Fixture;
import net.allegea.fixture.Rule;
import net.allegea.fixture.loader.TemplateLoader;
import net.allegea.fixture.model.Owner;

public class InnerClassTemplate implements TemplateLoader {

	public void load() {
		Fixture.of(Owner.class).addTemplate("valid", new Rule(){{
			add("inner", one(Owner.Inner.class, "valid"));
		}});

		Fixture.of(Owner.class).addTemplate("chained", new Rule(){{
		    add("inner.id", "333");
		}});

		Fixture.of(Owner.Inner.class).addTemplate("valid", new Rule(){{
			add("id", "222");
		}});
	}
}
