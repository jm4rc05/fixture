package net.allegea.fixture.template;

import net.allegea.fixture.Fixture;
import net.allegea.fixture.Rule;
import net.allegea.fixture.loader.TemplateLoader;
import net.allegea.fixture.model.Item;
import net.allegea.fixture.model.Order;
import net.allegea.fixture.model.Payment;

public class FixtureCircularTemplate implements TemplateLoader {

	public void load() {
		Fixture.of(Order.class).addTemplate("valid", new Rule(){{
			add("id", random(Long.class, range(1L, 200L)));
			add("items", has(3).of(Item.class, "valid"));
			add("payment", one(Payment.class, "valid"));
		}});

		Fixture.of(Order.class).addTemplate("otherValid", new Rule(){{
			add("id", random(Long.class, range(1L, 200L)));
			add("items", has(3).of(Item.class, "valid", "order"));
			add("payment", one(Payment.class, "valid", "order"));
		}});
		
		Fixture.of(Item.class).addTemplate("valid", new Rule(){{
			add("productId", random(Integer.class, range(1L, 200L)));
		}});

		Fixture.of(Payment.class).addTemplate("valid", new Rule(){{
			add("id", random(Long.class, range(1L, 200L)));
		}});
	}
}
