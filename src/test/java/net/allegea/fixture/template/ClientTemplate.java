package net.allegea.fixture.template;

import net.allegea.fixture.Fixture;
import net.allegea.fixture.Rule;
import net.allegea.fixture.loader.TemplateLoader;
import net.allegea.fixture.model.Address;
import net.allegea.fixture.model.City;
import net.allegea.fixture.model.Client;

public class ClientTemplate implements TemplateLoader {

	public void load() {
		Fixture.of(Client.class).addTemplate("valid", new Rule(){{
			add("id", random(Long.class, range(1L, 200L)));
			add("name", random("Anderson Parra", "Arthur Hirata"));
			add("nickname", random("nerd", "geek"));
			add("email", "${nickname}@gmail.com");
			add("birthday", instant("18 years ago"));
			add("address", one(Address.class, "valid"));
		}});
		
		Fixture.of(Client.class).addTemplate("valid-noaddress").inherits("valid", new Rule(){{
			add("address", null);
		}});
		
		Fixture.of(Address.class).addTemplate("valid", new Rule(){{
			add("id", random(Long.class, range(1L, 100L)));
			add("street", random("Paulista Avenue", "Ibirapuera Avenue"));
			add("city", one(City.class, "valid"));
			add("state", "${city}");
			add("country", "Brazil");
			add("zipCode", random("06608000", "17720000"));
		}});
		
		Fixture.of(Address.class).addTemplate("valid-augusta").inherits("valid", new Rule(){{
			add("street", "Augusta Street");
		}});
	}
}
