package net.allegea.fixture.template;

import java.math.BigDecimal;
import java.math.MathContext;
import java.text.SimpleDateFormat;

import net.allegea.fixture.Fixture;
import net.allegea.fixture.Rule;
import net.allegea.fixture.loader.TemplateLoader;
import net.allegea.fixture.model.Invoice;

public class InvoiceTemplate implements TemplateLoader {

	public void load() {
		Fixture.of(Invoice.class).addTemplate("valid", new Rule(){{
			add("id", regex("\\d{3,5}"));
			add("ammount", random(BigDecimal.class, new MathContext(2)));
			add("dueDate", beforeDate("2011-04-08", new SimpleDateFormat("yyyy-MM-dd")));
			
		}}).addTemplate("previousInvoices", new Rule(){{
			add("id", regex("\\d{3,5}"));
			add("ammount", random(BigDecimal.class, range(new BigDecimal("45.89"), new BigDecimal("58.67"))));
			add("dueDate", sequenceDate("2011-04-01", new SimpleDateFormat("yyyy-MM-dd"), decrement(1).month()));
			
		}}).addTemplate("nextInvoices", new Rule(){{
			add("id", regex("\\d{3,5}"));
			add("ammount", random(new BigDecimal("58.67"), new BigDecimal("45.89")));
			add("dueDate", sequenceDate("2011-04-30", increment(1).day()));
		}});
	}
}
