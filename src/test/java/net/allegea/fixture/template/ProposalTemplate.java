package net.allegea.fixture.template;

import net.allegea.fixture.Fixture;
import net.allegea.fixture.Rule;
import net.allegea.fixture.loader.TemplateLoader;
import net.allegea.fixture.model.SimpleProposal;

public class ProposalTemplate implements TemplateLoader {

	public void load() {
		Fixture.of(SimpleProposal.class).addTemplate("valid", new Rule(){{
			add("item.order.id", "123456");
		}});
	}

}
