package net.allegea.fixture.template;

import net.allegea.fixture.Fixture;
import net.allegea.fixture.Rule;
import net.allegea.fixture.loader.TemplateLoader;
import net.allegea.fixture.model.XMLTransaction;

public class XMLTransactionTemplate implements TemplateLoader {

	public void load() {
		Fixture.of(XMLTransaction.class).addTemplate("validTransaction", new Rule(){{
			add("origin", "ORIGIN");
			add("date", instant("now"));
			add("hour", instant("now"));
		}});
	}
}
