package net.allegea.fixture.template;

import net.allegea.fixture.Fixture;
import net.allegea.fixture.Rule;
import net.allegea.fixture.loader.TemplateLoader;
import net.allegea.fixture.model.User;
import net.allegea.regex.Gender;

public class UserTemplate implements TemplateLoader {

	public void load() {
		Fixture.of(User.class).addTemplate("anyValidUser", new Rule(){{
			add("name", name());
			add("login", random("login1", "login2"));
			add("password", "madona");
			add("gender", random(Gender.class));
			add("email", "${login}@gmail.com");
		}}
		).addTemplate("validFemaleUser", new Rule(){{
			add("name", name(Gender.FEMALE));
			add("login", "${name}");
			add("password", name(Gender.MALE));
			add("gender", Gender.FEMALE);
			add("email", "duck@gmail.com");
		}});
	}
}
