package net.allegea.fixture.model;

public class SimpleProposal extends Proposal {

	private Item item;

	public Item getItem() {
		return item;
	}

	public void setItem(Item item) {
		this.item = item;
	}
}
