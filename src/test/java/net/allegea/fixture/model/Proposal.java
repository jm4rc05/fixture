package net.allegea.fixture.model;

public abstract class Proposal {
	
	private String id;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
}
