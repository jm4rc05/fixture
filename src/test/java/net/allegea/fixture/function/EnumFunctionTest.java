package net.allegea.fixture.function;

import static org.junit.Assert.assertEquals;

import java.util.List;

import net.allegea.fixture.model.UserType;

import org.junit.Test;

public class EnumFunctionTest {
	
	@Test
	public void shouldGenerateRandomEnumValues() {
		List<UserType> userTypes = new EnumFunction(UserType.class, 2).generateValue();
		
		assertEquals(2, userTypes.size());
	}
}
