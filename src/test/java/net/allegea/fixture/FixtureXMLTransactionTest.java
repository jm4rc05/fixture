package net.allegea.fixture;

import static junit.framework.Assert.assertNotNull;
import net.allegea.fixture.loader.FixtureFactoryLoader;
import net.allegea.fixture.model.XMLTransaction;

import org.junit.BeforeClass;
import org.junit.Test;

public class FixtureXMLTransactionTest {

	@BeforeClass
	public static void setUp() {
		FixtureFactoryLoader.loadTemplates("net.allegea.fixture.template");
	}

	@Test
	public void fixtureValidTransaction() {
		XMLTransaction transaction = Fixture.from(XMLTransaction.class).gimme("validTransaction");
		assertNotNull("User should not be null", transaction);
	}
	
}
