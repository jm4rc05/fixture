package net.allegea.fixture.transformer;

import net.allegea.fixture.util.ReflectionUtils;

public class PropertyPlaceholderTransformer extends PlaceholderTransformer {
    
    private final Object result;
    
    public PropertyPlaceholderTransformer(final Object result) {
        this.result = result;
    }

    @Override
    protected String getValue(String propertyName) {
        return ReflectionUtils.invokeRecursiveGetter(result, propertyName).toString();
    }
}