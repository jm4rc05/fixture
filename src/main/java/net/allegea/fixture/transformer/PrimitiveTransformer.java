package net.allegea.fixture.transformer;

import java.util.Arrays;

import net.allegea.fixture.util.ReflectionUtils;

import org.apache.commons.lang.ClassUtils;

public class PrimitiveTransformer implements Transformer {

    @SuppressWarnings("unchecked")
    public <T> T transform(Object value, Class<T> type) {
        return (T) ReflectionUtils.newInstance(ClassUtils.primitiveToWrapper(type), Arrays.asList(value));
    }

    public boolean accepts(Object value, Class<?> type) {
        return value instanceof String && type.isPrimitive();
    }
}
