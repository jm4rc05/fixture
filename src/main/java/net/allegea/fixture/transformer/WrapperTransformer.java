package net.allegea.fixture.transformer;

import java.util.Arrays;

import net.allegea.fixture.util.ReflectionUtils;

public class WrapperTransformer implements Transformer {

    public <T> T transform(Object value, Class<T> type) {
        return ReflectionUtils.newInstance(type, Arrays.asList(value));
    }

    public boolean accepts(Object value, Class<?> type) {
        return value instanceof String && (Number.class.isAssignableFrom(type) || Boolean.class.isAssignableFrom(type));
    }
}