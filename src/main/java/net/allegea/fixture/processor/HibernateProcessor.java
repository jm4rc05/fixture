package net.allegea.fixture.processor;

import org.hibernate.Session;

public class HibernateProcessor implements Processor {

    private Session session;
    
    public HibernateProcessor(Session session) {
        this.session = session;
    }
    
    public void execute(Object result) {
        session.save(result);
    }
    
}
