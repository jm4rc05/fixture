package net.allegea.fixture.processor;

public interface Processor {

    void execute(Object result);
    
}
