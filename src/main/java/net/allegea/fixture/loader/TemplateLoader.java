package net.allegea.fixture.loader;

public interface TemplateLoader {

	void load();
	
}
