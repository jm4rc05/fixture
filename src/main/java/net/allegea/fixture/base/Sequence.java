package net.allegea.fixture.base;

public interface Sequence<T> {

	T nextValue();
	
}
