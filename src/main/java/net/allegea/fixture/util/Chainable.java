package net.allegea.fixture.util;

import net.allegea.fixture.function.Function;

public interface Chainable {

	Function of(Class<?> clazz, String label);
	
	Function of(Class<?> clazz, String label, String targetAttribute);

	Function of(Class<? extends Enum<?>> clazz);
	
}
