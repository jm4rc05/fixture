package net.allegea.fixture.function;

import net.allegea.regex.RegexGen;

public class RegexFunction implements AtomicFunction {

	private String pattern;
	
	public RegexFunction(String pattern) {
		this.pattern = pattern;
	}

	@SuppressWarnings("unchecked")
	public <T> T generateValue() {
		return (T) RegexGen.of(pattern);
	}

}
