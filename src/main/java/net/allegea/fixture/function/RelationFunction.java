package net.allegea.fixture.function;

import net.allegea.fixture.processor.Processor;


public interface RelationFunction extends Function {

	<T> T generateValue(Processor processor);
	
	<T> T generateValue(Object owner);
	
	<T> T generateValue(Object owner, Processor processor);
	
}
