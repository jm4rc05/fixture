package net.allegea.fixture.function;

import net.allegea.fixture.base.Sequence;

public class SequenceFunction implements AtomicFunction {

	private Sequence<?> sequence;
	
	public SequenceFunction(Sequence<?> sequence) {
		super();
		this.sequence = sequence;
	}

	@SuppressWarnings("unchecked")
	public <T> T generateValue() {
		return (T) sequence.nextValue();
	}
}
