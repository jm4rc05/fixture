package net.allegea.fixture.function;

public class IdentityFunction implements AtomicFunction {
    
    private Object value;
    
    public IdentityFunction(Object value) {
        this.value = value;
    }
    
   @SuppressWarnings("unchecked")
	public <T> T generateValue() {
		return (T) this.value;
	}
}
