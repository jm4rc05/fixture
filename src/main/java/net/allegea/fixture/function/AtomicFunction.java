package net.allegea.fixture.function;


public interface AtomicFunction extends Function {

	<T> T generateValue();
	
}
